Willkommen beim Pilotsystem **Offene Daten Mettmann** - dem Portal für Mettmann gemäß der [Open Data](http://de.wikipedia.org/wiki/Open_Data) und [Open Government Data] (http://de.wikipedia.org/wiki/Open_Government_Data) Bewegung.

Dieses Projekt der Piraten in Mettmann befindet sich in der Pilotphase. Weitere Infos unter [FAQ](/pages/faq).