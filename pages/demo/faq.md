##Motivation und Hintergrund

###Was ist Sinn und Zweck des Portals "Offene Daten Mettmann"?

__Unsere Vision ist:__

* Die politischen Prozesse in Mettmann transparenter zu gestalten und auf lange Sicht eine politische Beteiligung der Bürger zu ermöglichen.
* Mehr Licht ins Dunkel zu bringen.
* Dass den Mettmanner Bürgern ein Licht aufgeht.

__Unsere Ziele sind:__

* Wir wollen vereinfachen.

    Die komplexe und schlecht dokumentierte Arbeit des Stadtrats werden wir nachvollziehbarer machen und damit die Basis schaffen, fundierte (Bürger-) Beteiligung zu ermöglichen.

* Wir wollen informieren

    In diesem Portal sollen möglichst viele politisch relevanten Vorgänge und Informationen dem Bürger transparent und neutral (aber nicht unkritisch) zugänglich gemacht werden.

    Die Plattform soll zeigen, was moderne Bürger von einer transparenten Kommune erwarten.

* Wir wollen zur Beteiligung anregen

    Durch die transparente Abbildung bestimmter Arbeitsabläufe, Entscheidungsprozesse und Ergebnisse sollen politisch interessierte und im digitalen Zeitalter angekommene Bürger angeregt und befähigt werden, sich an kommunalen Entscheidungsprozessen (direkt) zu beteiligen.

* Wir wollen Werte vermitteln

    Wir fördern bei Mettmanner Bürgern das Verständnis für Werte, die die direkte Beteiligung fördern und fordern:

    * Teile Wissen - Wissen ist Macht!
    * Verstehe das System!
    * Denke einfach!
    * Mach's selbst!
    * Sei kritisch!
    * Sei neugierig!

    Wir zeigen anhand der Möglichkeiten und Auswirkungen der digitalen Revolution auf, dass mit Wissen und Transparenz eine direkte Teilhabe auch in kleineren Kommunen möglich ist.

###Was ist die Idee hinter "Offenen Daten" bzw. "Open Data"?

[Offene Daten](http://de.wikipedia.org/wiki/Open_Data) sollen mehr Transparenz, Teilhabe und Zusammenarbeit ermöglichen.

Zitat aus der [Wikipedia](http://de.wikipedia.org/wiki/Open_Data):
_Offene Daten sind sämtliche Datenbestände, die im Interesse der Allgemeinheit der Gesellschaft ohne jedwede Einschränkung zur freien Nutzung, zur Weiterverbreitung und zur freien Weiterverwendung frei zugänglich gemacht werden._

Gemäß der [Open Data Richtlinien](http://okfn.de/opendata/) sind die Hauptmerkmale von Open Data:

* Die Daten sind frei verfügbar und frei zugänglich.
* Die Daten müssen frei wiederverwendet werden können.
* Die Daten müssen diskriminierungsfrei von jeder Person nutzbar und wiederverwendbar sein.

Dabei geht es auch und vor allem um Daten, die die Allgemeinheit bereits finanziert hat. Oft kommt es vor (z.B. in der Forschung, in den Medien oder in der Politik), dass solche Daten trotz allgemeinen Interesses und trotz Finanzierung durch den Steuerzahler gar nicht, nicht vollständig oder nur unter einschränkenden Bedingungen offengelegt werden.

###Was bedeutet "Open Government"?

Als besondere Form von Open Data wird bei [Open Government](http://de.wikipedia.org/wiki/Open_Government) das Augenmerk auf die Daten gelegt, die in unseren Regierungen und Verwaltungen - also der öffentliche Hand - als [Open Government Data](http://de.wikipedia.org/wiki/Open_Government_Data) anfallen. Eine Öffnung dieser Daten soll zu mehr Transparenz und Bürgerbeteiligung führen und politische Entscheidungen offener und nachvollziehbar machen.

Nicht nur die Bürger, sondern auch die Verwaltungen selbst sollen davon profitieren, da beide möglichst auf Augenhöhe agieren können und die Interessen der Allgemeinheit frühzeitig eingebunden werden können.

###An wen wendet sich dieses Portal?

Die Daten und Informationen in diesem Portal sollen interessierten Bürgern helfen, politische und verwaltungstechnische Vorgänge in Mettmann besser zu verstehen. Außerdem glauben wir, dass auch Entscheidungsträger und Mitarbeiter in Politik und Verwaltung ihren Nutzen aus dieser Plattform ziehen können, sei es durch die Bereitstellung weiterer Daten oder die Nutzung der Features dieses Portals bei der täglichen Arbeit.

Neben Politik und Verwaltung können wir uns weitere Datenlieferanten vorstellen, wie z.B. Vereine, Verbände oder gar Unternehmen in Mettmann.

Wir richten uns auch an Entwickler aus dem privaten oder geschäftlichen Umfeld, die Anwendungen zur Weiterverwendung der Daten erstellen können.

###Wer betreibt dieses Portal?

Dieses Portal wird betrieben und redaktionell betreut durch die [Piraten in Mettmann] (https://blog.piratenpartei-nrw.de/mettmann/).

###Wie ist dieses Portal entstanden?

Die Initiatoren dieses Projekts beobachten die Open Data Szene schon eine ganze Weile und waren von der Möglichkeit fasziniert, die politischen Prozesse in Mettmann transparenter zu gestalten und für die Bürger greifbarer zu machen. Von daher war es naheliegend, in einem ersten Schritt die Daten aus dem Ratsinformationssystem so aufzubereiten und anzubieten, dass sie ihren Zweck besser erfüllen können.

###In welcher Beziehung steht dieses Portal zur Stadt Mettmann?

Wir haben dieses Portal ohne vorherige Absprache mit der Stadt Mettmann erstellt. Wir befürchten, dass die Stadt Mettmann weder die finanziellen oder personellen Ressourcen noch das Know-how hat, um eigenständig ein Open Data Portal auf die Beine zu stellen. Eine Einbindung der Stadtverwaltung hätte unseres Erachtens das Projekt jedenfalls verzögert oder gar unmöglich gemacht.

Gleichwohl können wir uns vorstellen, die Stadt Mettmann mit ins Boot zu holen. Wir sehen uns als Initiator dieses Projekts, der helfen möchte, in Mettmann eine Open Data Plattform zu etablieren. Wir glauben, dass wir mit unserem Beitrag bereits einen Großteil der administrativen Arbeit zum Start dieser Plattform abgenommen haben.

Auf der anderen Seiten möchten wir auch darauf hinweisen, dass Daten und Informationen nach der Open Data Richtlinie in erster Linie den Bürgern gehören und diesen einen Mehrwert bieten müssen. Daher werden Unabhängigkeit und Transparenz unseres Erachtens am besten gewährleistet, wenn Datenlieferanten und Betreiber nicht in einer Hand liegen. In welcher Form dies in Zukunft gestaltet werden kann, können wir gerne diskutieren.

###Welche Vorteile hat dieses Portal gegenüber dem Ratsinformationssystem der Stadt Mettmann?

Zuerst einmal möchten wir darauf hinweisen, dass wir die Dokumente im Ratsinformationssystem der Stadt Mettmann nicht in diesem Portal speichern, sondern in den Datensätzen lediglich darauf verlinken.

Dabei bietet dieses Portal folgende Features und Vorteile:

* Die verwendete Software ist auf die Belange der Open Data Bewegung optimiert, vor allem was Offenheit, Erweiterbarkeit und Datenverarbeitung angeht.
* Dokumente aus dem Ratsinformationssystem können verlinkt werden, um sie z.B. in E-Mails zu verschicken. Das Ratsinformationssystem selbst "versteckt" die Links.
* Dokumente aus dem Ratsinformationssystem und andere Daten werden eingebettet direkt im Browser angezeigt.
* Eine direkte Verlinkung der Datensätze in den Sozialen Netzwerken Google+, Twitter und Facebook ist möglich.
* Die Daten im Portal können über eine [API](http://docs.ckan.org/en/latest/api.html) abgerufen werden. Das ist wichtig für die Weiterverwendung offener Daten.
*  Bürger werden durch eine Kommentarfunktion eingebunden und können Feedback geben. Das ist als Vorstufe für eine mögliche Bürgerbeteiligung zu sehen.
* Datensätze können verschlagwortet und so thematisch gegliedert werden. Das erleichtert eine Nachvollziehbarkeit von thematisch oder zeitlich zusammenhängenden Vorgängen.
* Über "Kategorien" können übergeordnete Gruppierungen erstellt werden.
* Eindeutige Lizenzangaben könne für jeden Datensatz individuell getätigt werden.
* In den [Metadaten](https://de.wikipedia.org/wiki/Metadaten) können strukturiert Angaben zu den Datensätzen gemacht werden, so z.B. eine Quellangabe, Ansprechpartner und Datenformat.
* Metadaten und Schlagworte sind durchsuchbar.
* Über die "Aktivitätsanzeige" kann verfolgt werden, wer wann Änderungen an den Datensätzen gemacht hat. Änderungen an den Datensätzen selbst, den sog. Ressourcen, können nachverfolgt werden (Versionsverwaltung).

###Welche vergleichbaren Portale gibt es?

Viele Städte und Gemeinden, aber auch Bund und Länder haben bereits Open Data Portale gegründet. Kleinere und mittelgroße Städte haben es dabei ungleich schwieriger, die nötigen Ressourcen zu stellen und das fachliche Know-How zu bündeln. Von daher sind die größeren Städte oftmals heute schon vertreten. Exemplarisch seien hier einige Open Data Plattformen von Städten genannt:

* [Offene Daten Berlin](http://daten.berlin.de)
* [Open Data Hamburg](http://daten.hamburg.de)
* [Offene Daten Moers](http://offenedaten.moers.de)
* [Offenes Köln](http://offeneskoeln.de)
* [OpenRuhr](http://openruhr.de/openruhrris/) mit Bochum, Duisburg und Moers
* [Hamburg Mitte Dokumente](http://www.hamburgmittedokumente.de)

Einen übergeordneten Datenkatalog findet man unter [offenedaten.de](https://offenedaten.de).

Wir möchten anmerken, dass bei vielen dieser Plattformen, vor allem auf Bund- und Landesebene, statistische Daten und Zahlen z.B. aus den Bereichen Arbeitsmarkt, Demographie, Umwelt und Verkehr im Vordergrund stehen. Dies sind die "klassischen" Daten für Open Data, die in den meisten Verwaltungen und amtlichen Behörden vorhanden sind. Diese Portale werden meist durch die Verwaltungen selbst betrieben - oft unter Einbeziehung lokaler Unternehmen oder Forschungseinrichtungen.

Bisher gibt es eher wenige Portale, die sich auf politische Entscheidungen und Beschlüsse aus den Stadt- oder Kreisräten konzentrieren. Beispiele dafür sind "Offenes Köln" oder "OpenRuhr", welche von Privatpersonen oder Institutionen der Open Data Bewegung betrieben werden. Es scheint so, dass besonders bei der Öffnung politischer Entscheidungsprozesse die Städte und Gemeinden noch Nachholbedarf haben, sich selbst zu engagieren.

###Welche Daten werden eingestellt?

Zum Start der Plattform konzentrieren wir uns auf die Daten aus dem [Ratsinformationssystem](https://mettmann.more-rubin1.de) der Stadt Mettmann.

###Gibt es Pläne, zusätzliche bzw. andere Daten einzustellen?

Für weitere Daten sind wir offen. Es gibt keine konkreten Pläne, aber schon einige Ideen. Feedback und Hilfe sind [willkommen](/pages/kontakt).

##Nutzung dieses Portals

###Was hat es mit der Kommentarfunktion auf sich?

Mit Kommentaren können Besucher des Portals sachliche Anmerkungen zu den Datensätzen machen.

Die Kommentarfunktion für Besucher wird über den externen Dienstleister [Disqus] (http://disqus.com) abgewickelt. Wir weisen ausdrücklich auf die im [Impressum](/pages/impressum) gemachten Angaben zu Datenschutz und Nutzungsbedingungen hin.

###Was bedeuten die Felder in den Datensätzen?

Diese Felder umfassen die sog. Metadaten.

* Titel

    Kurzer Titel des Datensatzes, meist aus dem Original übernommen.

* Beschreibung

    Eine Beschreibung des Datensatzes, meist aus dem Original übernommen.

* Schlagworte

    Siehe unten.

* Lizenz

    Hier steht die Lizenz für die Daten (siehe weitere Angaben in dieser FAQ).

* Organisation

    Siehe unten.

* Autor

    Hier steht die Angabe zum Datenbereitsteller, also dem Urheber der Daten.

* Status

* Datum

    Hier übernehmen wir die Angabe zum Zeitstempel aus der Datenquelle.

* Quelle

    Hier verlinken wir die Quelle der (Original-) Daten.

* Kategorie

    Siehe unten.

###Was sind "Organisationen", "Kategorien" und "Schlagworte"?

Die in diesem Portal hinterlegten Daten werden einer sog. "Organisation" zugeordnet bzw. von registrierten Anwendern dieser Organisation eingetragen. Dazu ist ein Account in diesem Portal nötig. 

Über "Kategorien" können übergeordnete Gruppierungen erstellt werden, z.B. "Ratsinformationssystem".

Über Schlagworte (Tage) können Datensätze thematisch gegliedert werden, z.B. in "Finanzen" oder "Schulen".

###Was verbirgt sich hinter den unter "Sozial" aufgeführten Links?

Hiermit ist eine direkte Verlinkung der Datensätze in den Sozialen Netzwerken Google+, Twitter und Facebook ist möglich.

##Lizenzen und Nutzung der Daten

###Unter welcher Lizenz stehen die Daten?

Die Nutzung des jeweiligen Datensatzes erfolgt unter der in den Metadaten jeweils genannten Lizenz der Datenbereitsteller.

Hat der Datenbereitsteller, in unserem Fall die Stadt Mettmann für das Ratsinformationssystem, keine Lizenz im Originaldatensatz angegeben, vergeben wir pauschal die [Creative Commons Attribution](http://creativecommons.org/licenses/by/3.0/de/) Lizenz, abgekürzt "CC BY".

Dies scheint uns unter Berücksichtigung der in der Open Data Szene vorherrschenden Lizenzierung die beste Wahl zu sein, siehe _"Kapitel 8.3.1 Einheitliche und einfache Lizenz- und Nutzungsbestimmungen"_ der Berliner Open Data Strategie [1].

Vielfach wird sogar eine [CC0 bzw. CCZero](http://wiki.creativecommons.org/De:CC0) Lizenz verwendet (also als Public Domain). Wir meinen jedoch, dass eine Namensnennung als Anerkennung und zur Angabe des Autors angebracht ist. Am Beispiel der Daten aus dem Ratsinformationssystem muss also die Stadt Mettmann als Autor genannt werden.

Diese "CC BY" Lizenz besagt:

* Teilen — Sie dürfen den Inhalt kopieren, verbreiten und zugänglich machen,
Abwandlungen und Bearbeitungen des Werkes bzw. Inhaltes anfertigen und
den Inhalt kommerziell nutzen.
* Namensnennung — Sie müssen den Namen des Autors/Rechteinhabers in der von ihm festgelegten Weise nennen.

Hinweis: Man könnte auf die Idee kommen, dass das Recht "Bearbeitungen des Werkes bzw. Inhaltes" anzufertigen bei kommunalpolitischen Daten unangebracht sei und deshalb eine [CC BY-ND](http://creativecommons.org/licenses/by-nd/3.0/de/) Lizenz besser sei. Diese Lizenz entspricht jedoch nicht den Open Data Richtlinien. Hier kommt es eben nicht darauf an, Daten einfach verändern zu dürfen im Sinne von "inhaltlich fälschen", sondern auf das Recht, Daten aus verschiedenen Quellen oder nur Teile daraus zusammenfügen zu dürfen (sog. Remix). Eine CC BY-ND Lizenz würde eine Wiederverwendung der Daten nur als Ganzes erlauben, was einer Wertschöpfung im Wege steht.

Merke: Selbst wenn bei "CC  BY" jemand die Daten "inhaltlich" verändern würde (also eine inhaltliche Abwandlung erstellt), müssen der Autor und der Link zu den Originaldaten angegeben werden.

###Wie und unter welchen Bedingungen kann ich die Daten nutzen?

Das hängt von der Lizenz ab. Siehe Erläuterungen dazu weiter oben.

Generell ist es Sinn und Zweck von Open Data, die Daten nicht nur zu erheben, sondern dieser für die allgemeine Nutzung in möglichst maschinenlesbarer Form anzubieten. Eine Wiederverwendung ist also auf die richtige Anwendung (Applikation) angewiesen. Die offene Lizenzierung ermöglicht es jeder Person, die Daten aufzubereiten. So könnte man es sich z.B. vorstellen, eine Smartphone-App zu programmieren, die die Daten aus diesem Portal zum Ratsinformationssystem so aufbereitet, dass man die Vorlagen und Beschlüsse auch unterwegs einfach einsehen kann.

###Darf ich die Daten kommerziell weiterverwenden?

Im Allgemeinen ja. Das hängt von der Lizenz ab. Siehe Erläuterungen dazu weiter oben.

###Gibt es Beispiele für die Weiterverwendung der Daten?

Nein, derzeit gibt es keine Anwendung, die Daten aus diesem Portal verwenden würde. Wir sind offen für Ideen und Mithilfe.

Welche Möglichkeiten Open Data Applikationen bieten, kann man beispielsweise bei [Offene Daten Berlin](http://daten.berlin.de/anwendungen) sehen. Das Portal [Apps für Deutschland](http://apps4deutschland.de/apps/) listet Internetapplikationen oder mobilen Applikationen, die auf offenen Daten der öffentlichen Hand basieren.

###Welche Daten werden nicht veröffentlicht?

Daten, die eindeutig gegen den Datenschutz und gegen Urheberrechte verstoßen, werden nicht veröffentlicht bzw. verlinkt.

Hinweis: Daten im Ratsinformationssystem werden generell nur verlinkt. Wir gehen davon aus, dass alle im Ratsinformationssystem veröffentlichten Daten frei vor Urheberrechten Dritter sind bzw. entsprechend urheberrechtlich gekennzeichnet sind und nicht gegen den Datenschutz verstoßen. Sollte sich ausnahmsweise herausstellen, dass diese Daten gegen den Datenschutz oder Urheberrechte verstoßen, werden wir die Links auf die jeweiligen Datensätze nach entsprechendem Hinweis entfernen. Hinweise dazu bitte über [Kontakt](/pages/kontakt). Siehe auch unsere Erklärungen im [Impressum](/pages/impressum). Es sollte jedoch bei reiner Verlinkung klar sein, dass vermeintliche Verstöße nicht durch uns, sondern durch den Betreiber des Ratsinformationssystems oder anderer externer Veröffentlichungen verursacht werden.

##Kontakt

###An wen wende ich mich bei generellen Fragen oder Anregungen?

Bei Fragen, Anmerkungen, Anregungen oder Kritik zu Offene Daten Mettmann, wenden Sie sich bitte an den Betreiber dieses Portal. Siehe [Kontakt](/pages/kontakt). 

###Wie kann ich Offene Daten Mettmann durch Mitarbeit unterstützen?

Wir sind dankbar für Ideen und Hilfe. Offen ist derzeit z.B., welche weiteren Daten wie eingestellt werden können oder wie eine Weiterverwendung von Daten erfolgen kann?

Die Piraten in Mettmann [treffen sich regelmäßig](https://blog.piratenpartei-nrw.de/mettmann/termine/) auf einem Stammtisch und in einem Arbeitskreis. Interessierte Bürger oder gesellschaftliche Gruppen sind herzlich willkommen, dort mit uns über diese Plattform zu diskutieren oder uns gar zu helfen.

Potentielle Datenlieferanten können sich gerne mit uns in Verbindung setzen, damit wir die nötigen technischen und organisatorischen Voraussetzungen schaffen können. Siehe auch Frage _"Wie kommen die Daten in das Portal?"_ weiter unten.

###Wer ist Ansprechpartner für die Daten?

Für die Erstellung und Speicherung der [Metadaten](https://de.wikipedia.org/wiki/Metadaten) mit den Links zum Ratsinformationssystem ist der [Betreiber dieses Portals](/pages/kontakt) erster Ansprechpartner.

Der eigentliche Urheber der Daten wird im jeweiligen Datensatz im Feld "Autor" genannt. Für Daten im Ratsinformationssystem ist das die "Stadt Mettmann".

##Technik

###Auf welcher Software basiert dieses Portal?

Wie verwenden das maßgeblich von der [Open Knowledge Foundation](http://okfn.org) entwickelte [CKAN](http://okfn.org). CKAN ist als Open Source [veröffentlicht](https://github.com/okfn/ckan) und wird bereits von vielen Open Data Portalen verwendet.
Zur Erweiterung der CKAN-Oberfläche und zur Datengewinnung aus dem Ratsinformationssystem haben wir die beiden folgenden kleinen Open Source Projekte erstellt:

* [ODME Theme](https://github.com/formwandler/ckanext-odmetheme) für Offene Daten Mettmann als CKAN-Erweiterung
* [ODME Tools](https://github.com/formwandler/odmetools) für Offene Daten Mettmann

###In welchen Formaten sind die Daten vorhanden?
Die Daten im Ratsinformationssystem liegen im PDF-Format vor und werden von uns verlinkt. Der im Portal eingebaute PDF-Betrachter ermöglicht die direkte Einsicht in die Dokumente im Browser.

Daten, die den Kriterien von Open Data genügen sollen, müssen strukturiert und maschinenlesbar zur Verfügung gestellt werden. PDF gehört eigentlich nicht dazu, wird aber aufgrund seiner Verbreitung unterstützt.

Strukturierte Daten sollten vielmehr in den Formaten XML, CSV, JSON, oder RDF vorliegen. Außerdem werden unterstützt: HTML, TXT, XLS (Excel), PDF, PNG und JPEG. Geoformate sind KML, GPX und GeoJSON.

Der in CKAN eingebaute [Viewer](http://docs.ckan.org/en/latest/data-viewer.html) zeigt die meisten dieser Formate im Browser eingebettet an.

###Wie kommen die Daten in das Portal?
Daten aus dem Ratsinformationssystem werden mit Hilfe unseres [ODME Tools](http://tools.offene-daten-mettmann.de) derzeit noch manuell in dieses Portal übertragen. Dabei werden sogenannte Metadaten wie Name, Beschreibung, Links zu den Dokumenten, Quellangaben und Schlagwörter teils automatisch aus dem Ratsinformationssystem extrahiert.

Die in diesem Portal hinterlegten Daten werden einer sog. "Organisation" zugeordnet bzw. von registrierten Anwendern dieser Organisation eingetragen. Dazu ist ein Account in diesem Portal nötig.

Daten aus dem Ratsinformationssystem werden derzeit durch die "Piraten in Mettmann" eingetragen.

Wir sind gerne bereit, für interessierte Gruppen (Behörden der Stadt Mettmann, Vereine, Verbände, Parteien, etc.) eigene "Organisationen" und Accounts in diesem Portal anzulegen. So könnten Daten von allgemeinem Interesse, die der [Open Data Richtlinien](http://okfn.de/opendata/) entsprechen, verlinkt oder veröffentlicht werden.
Mehr zu den technischen Hintergründen in der [CKAN Dokumentation](http://docs.ckan.org/en/latest/authorization.html).

## Referenzen
[1] [Berliner Open Data Strategie](http://de.slideshare.net/ProjektZukunft/berliner-opendatastrategie): _Organisatorische, rechtliche und technische Aspekte offener Daten in Berlin. Konzept, Pilot und Handlungsempfehlungen._ Auch als [Kurzfassung (PDF)](http://www.berlin.de/projektzukunft/fileadmin/user_upload/pdf/sonstiges/Berliner_Open_Data_Strategie_2012_kurz.pdf) oder bei [Amazon](http://www.amazon.de/gp/product/3839603684/) erhältlich.
