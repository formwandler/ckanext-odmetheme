##Technischer und redaktioneller Betrieb

![Logo der Piraten in Mettmann](/images/logo_piratenpartei_me_kopfzeile_grau_100.jpg "Piraten in Mettmann")

Dieses Portal [Offene Daten Mettmann] (http://www.offene-daten-mettmann.de) wird betrieben und redaktionell betreut durch die [Piraten in Mettmann] (https://blog.piratenpartei-nrw.de/mettmann/).

Vor Kontaktaufnahme mit dem u.g. Anbieter nach §5 TMG bitten wir zur Klärung von Fragen und Sachverhalten um direkte Kontaktaufnahme mit den Piraten in Mettmann per E-Mail unter der Adresse

    info[at]offene-daten-mettmann.de

Unter dieser Adresse freuen wir uns auch auf Ideen und Anregungen. Siehe auch [Kontakt](/pages/kontakt).

##Anbieter im Sinne des Telemediengesetzes

Dienstanbieter dieser Seite ist die Piratenpartei Nordrhein-Westfalen.

Verantwortlicher gemäß §5 TMG ist Michele Marsching.

Postanschrift:

    Landesverband Nordrhein-Westfalen
    Piratenpartei Deutschland
    Selbecker Str. 22
    40472 Düsseldorf

Digitale Kontaktaufnahme:

    E-Mail: vorstand [at] piratenpartei-nrw [punkt] de

Ladungsfähige Anschrift:
 
    Piratenpartei Deutschland
    Pflugstraße 9a
    10115 Berlin

##Rechtliche Hinweise zu den angebotenen Daten und Informationen

[Offene Daten Mettmann] (http://www.offene-daten-mettmann.de) ist keine offizielle Quelle für amtliche Dokumente und behördliche Informationen. Trotz großer Anstrengungen, korrekte und vollständige Daten einzustellen, kann es zu Fehlern und Lücken im Datenbestand kommen.

Für die Richtigkeit der Daten und Inhalte übernimmt der Betreiber von Offene Daten Mettmann keinerlei Gewährleistung oder Garantie. Die Nutzung von Offene Daten Mettmann geschieht ausschließlich auf eigene Verantwortung der Nutzerinnen und Nutzer.

Wir weisen im besonderen darauf hin, dass die hier angebotenen Daten aus dem Ratsinformationssystem ausschließlich verlinkt werden zum offiziellen [Ratsinformationssystem der Stadt Mettmann] (https://mettmann.more-rubin1.de). Dabei werden die externen Daten - wenn technisch möglich - im Browser des Besuchers eingebettet angezeigt, z.B. in einem PDF-Betrachter oder als Tabelle. Diese Form des im Browser eingebetteten Inhalts bedeutet nicht, dass wir uns die Daten zu eigen machen, sondern soll lediglich dazu dienen, den Zugriff auf die extern gespeicherten Daten und Informationen zu erleichtern. Eine Datenspeicherung bei Offene Daten Mettmann ist in solchen Fällen auf Metadaten beschränkt (z.B. Quellangabe, Datum, Schlagworte).
Ob und inwieweit Daten nur verlinkt oder in Ausnahmefällen explizit bei Offene Daten Mettmann gespeichert sind, kann man jedem Datensatz einzeln entnehmen.

Sollten Sie sich auf hier eingestellte Dokumente verbindlich und offiziell beziehen wollen, erkundige Sie sich bitte bei den offiziellen Stellen (der Stadtverwaltung, dem Rat, den Gremien oder im Ratsinformationssystem) nach der aktuellen und amtlichen Version. Sollte es sich um Daten aus dem Ratsinformationssystem handeln, geben wir die Quelle im jeweiligen Datensatz an.

##Lizenzierung der Daten und Nutzungsbedingungen

Siehe [FAQ](/pages/faq) unter _Unter welcher Lizenz stehen die Daten?_.

##Datenschutz

__IP-Adressen__

Wir speichern standardmäßig einen Teil der IP-Adresse und weitere "Header-Daten" des Browsers, mit der diese Seite besucht wird, in einer Logdatei auf dem Server. Ein solcher Eintrag sieht z.B. folgendermaßen aus:

    178.201.196.0 - - [08/Dec/2013:14:39:15 +0100] "GET / HTTP/1.1" 200 5232 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:25.0) Gecko/20100101 Firefox/25.0"

Das letzte Byte der IP-Adresse wird dabei auf "0" gesetzt und soweit anonymisiert, so dass eine personenbezogene Nachverfolgung der Benutzeraktivitäten anhand dieser Logdatei nicht möglich ist. Dazu verwenden wir das Apache-Modul [mod_log_ipmask](http://www.saechsdsb.de/ipmask). Dies gilt für [IPv4](https://de.wikipedia.org/wiki/IPv4). Da wir derzeit keine [IPv6](https://de.wikipedia.org/wiki/IPv6) Anbindung haben, erübrigt sich eine Betrachtung für IPv6.

Die Logdateien sind nur für Administratoren dieser Webseite einsehbar und werden regelmäßig gelöscht. Wir werten die anonymisierten IP-Adressen nicht aus und stellen die Logdateien auch nicht Dritten zur Verfügung.

Ausnahme: Bei der aktiven Nutzung der Kommentarfunktion - also wenn der Besucher einen Kommentar schreibt - wird die komplette IP-Adresse an den externen Dienstleister geschickt und dort zusammen mit den im Kommentar angegebenen Daten gespeichert. Wird die Kommentarfunktion mit persönlicher Anmeldung genutzt, kann eine personenbezogene Nachverfolgung anhand der beim externen Dienstleister gespeicherten Daten theoretisch erfolgen.  Siehe auch Hinweise zur Kommentarfunktion.

__Kommentare__

Die Kommentarfunktion für Besucher wird über den externen Dienstleister [Disqus] (http://disqus.com) abgewickelt. Von daher weisen wir darauf hin, dass alle Kommentare bei diesem Dienstleister gespeichert werden. Die [Nutzungs- und Datenschutzbedingungen] (http://docs.disqus.com/kb/terms-and-policies) von Disqus sind zu beachten. Durch Verfassen von Kommentaren auf dieser Plattform wird diesen Bedingungen zugestimmt.

Eine Anmeldung bei Disqus ist für die Nutzung der Kommentarfunktion jedoch nicht erforderlich, es reicht die Angabe eines Namens und einer E-Mail-Adresse mit der Option _"Ich schreib lieber als Gast."_ . Beide Angaben werden inhaltlich nicht geprüft und können fiktiv sein, so dass mit einem Pseudonym eine weitgehend anonyme Nutzung der Kommentarfunktion erfolgen kann. Ein Auftreten unter "gefälschter Identität" ist allerdings unerwünscht.

Wie bei Foren und Blogs üblich, können die Administratoren von Offene Daten Mettmann alle Kommentare mit den bei Disqus hinterlegten Angaben zu Name, E-Mail Adresse und IP-Adresse einsehen. Eine Moderation der Kommentare bleibt vorbehalten.

Wir weisen darauf hin, dass wir uns vorbehalten, Kommentare zu löschen, die gegen die übliche [Netiquette] (https://de.wikipedia.org/wiki/Netiquette) oder gar gegen geltendes Recht verstoßen. Hinweise diesbezüglich bitte an die o.g. E-Mail Adresse oder über die in der Kommentarfunktion eingebaute Funktion zur Markierung unpassender Kommentare.

__Cookies__

Für normale Besucher werden im Browser des Anwenders keine Cookies von Offene Daten Mettmann gesetzt. Sobald jedoch eine Seite mit eingebetteter Kommentarfunktion aufgerufen wird, werden auf dem Rechner des Anwenders gemäß [Disqus Privacy Policy] (http://help.disqus.com/customer/portal/articles/466259-privacy-policy) Cookies gesetzt. Siehe auch Hinweise zur Kommentarfunktion.

__Datenschutzerklärung gemäß DSGVO__

Eine ausführliche Erklärung gemäß Datenschutzgrundverordnung (DSGVO) finden Sie in den [Datenschutzbestimmungen](https://www.piratenpartei-nrw.de/impressum/datenschutzerklaerung/) der Piratenpartei Nordrhein-Westfalen.

##Haftungshinweis

Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte externer Links. Für den Inhalt der verlinkten Seiten und Daten sind ausschließlich deren Betreiber verantwortlich.

##Sonstiges

Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.
