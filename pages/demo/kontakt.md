Dieses Demo-Portal [Offene Daten Mettmann] (http://demo.offene-daten-mettmann.de) wird betrieben und redaktionell betreut durch die [Piraten in Mettmann] (https://blog.piratenpartei-nrw.de/mettmann/).

Anmerkungen, Fragen und Wünsche können gerne per E-Mail an die folgende Adresse geschickt werden:

    info[at]offene-daten-mettmann.de


Die Piraten in Mettmann [treffen sich regelmäßig](https://blog.piratenpartei-nrw.de/mettmann/termine/) auf einem Stammtisch und in einem Arbeitskreis. Besucher sind herzlich willkommen.

Siehe auch unser [Impressum] (../pages/impressum) und die [FAQ] (../pages/faq).
