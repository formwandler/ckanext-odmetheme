**Offene Daten Mettmann Demo** ist eine Testumgebung für die Bereitstellung von offenen kommunalpolitischen Informationen und amtlichen Daten aus der Stadt Mettmann gemäß der [Open Data](http://de.wikipedia.org/wiki/Open_Data) und [Open Government Data] (http://de.wikipedia.org/wiki/Open_Government_Data) Bewegung.

**Achtung: Dies ist eine Testumgebung mit Dummy-Daten.**

Dieses Projekt der Piraten in Mettmann befindet sich in der Testphase. Weitere Infos unter [FAQ](/pages/faq).