Static Pages
============

This directory contains the source-code of the static pages managed by [ckanext-pages](https://github.com/okfn/ckanext-pages). The pages must be created by copying the code into the corresponding page managed within the CKAN GUI.

Created from and used by [Offene Daten Mettmann](http://www.offene-daten-mettmann.de).
