#!/bin/bash

# Inspired from https://github.com/open-data/ckanext-canada/blob/master/bin/build-combined-ckan-mo.sh

# TODO: The "en" version is currently copied to "en_GB", since there is no original "en" .mo file.
#       Maybe we need to change the CKAN code to load also an .mo file for "en".

HERE=`dirname $0`
msgcat --use-first \
    "$HERE/../ckanext/odmetheme/i18n/de/LC_MESSAGES/ckan.po" \
    "$HERE/../../ckan/ckan/i18n/de/LC_MESSAGES/ckan.po" \
    | msgfmt - -o "$HERE/../../ckan/ckan/i18n/de/LC_MESSAGES/ckan.mo"
msgcat --use-first \
    "$HERE/../ckanext/odmetheme/i18n/en/LC_MESSAGES/ckan.po" \
    "$HERE/../../ckan/ckan/i18n/en_GB/LC_MESSAGES/ckan.po" \
    | msgfmt - -o "$HERE/../../ckan/ckan/i18n/en_GB/LC_MESSAGES/ckan.mo"
msgcat --use-first \
    "$HERE/../ckanext/odmetheme/i18n/fr/LC_MESSAGES/ckan.po" \
    "$HERE/../../ckan/ckan/i18n/fr/LC_MESSAGES/ckan.po" \
    | msgfmt - -o "$HERE/../../ckan/ckan/i18n/fr/LC_MESSAGES/ckan.mo"
msgcat --use-first \
    "$HERE/../ckanext/odmetheme/i18n/it/LC_MESSAGES/ckan.po" \
    "$HERE/../../ckan/ckan/i18n/it/LC_MESSAGES/ckan.po" \
    | msgfmt - -o "$HERE/../../ckan/ckan/i18n/it/LC_MESSAGES/ckan.mo"