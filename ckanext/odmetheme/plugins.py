import ckan.plugins as p


class OdmeTheme(p.SingletonPlugin):
    """
    Plugin containing the theme for Offene Daten Mettmann
    """
    p.implements(p.IConfigurer)

    def update_config(self, config):
        # add template directory, see http://docs.ckan.org/en/latest/extensions/plugins-toolkit.html
        p.toolkit.add_template_directory(config, 'templates')
        p.toolkit.add_public_directory(config, 'public')
        p.toolkit.add_resource('fanstatic_library', 'ckanext-odmetheme')
