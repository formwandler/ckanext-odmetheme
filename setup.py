from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(
    name='ckanext-odmetheme',
    version=version,
    description="CKAN extension providing a theme for Offene Daten Mettmann",
    long_description="""\
    """,
    classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords='',
    author='Ralf Kruedewagen',
    author_email='git@kruedewagen.de',
    url='http://www.kruedewagen.de',
    license='Affero GPL',
    packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
    namespace_packages=['ckanext', 'ckanext.odmetheme'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        # -*- Extra requirements: -*-
    ],
    entry_points=\
    """
    [ckan.plugins]
    odmetheme=ckanext.odmetheme.plugins:OdmeTheme
    """,
)
